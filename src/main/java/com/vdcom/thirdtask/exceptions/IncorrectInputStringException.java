package com.vdcom.thirdtask.exceptions;

public class IncorrectInputStringException extends Exception {
    public IncorrectInputStringException(String message) {
        super(message);
    }
}
