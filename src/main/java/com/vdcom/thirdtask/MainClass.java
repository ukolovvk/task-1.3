package com.vdcom.thirdtask;

import com.vdcom.thirdtask.converter.Converter;
import com.vdcom.thirdtask.entity.ValueEquation;
import com.vdcom.thirdtask.exceptions.IncorrectInputStringException;
import com.vdcom.thirdtask.converter.ConverterImpl;
import com.vdcom.thirdtask.utils.Parser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MainClass {

    public static void main(String[] args) {
        System.out.println("Input equations: (empty line is end of input)");
        List<ValueEquation> equations = new ArrayList<>();
        List<ValueEquation> equationsToBeFound = new ArrayList<>();
        readAndParseEquationsFromConsole(equations, equationsToBeFound);
        Converter converter = new ConverterImpl(equations);
        System.out.println("-----------------");
        equationsToBeFound.forEach(eq -> {
            ValueEquation convertedEquation = converter.convert(eq.getFirstValue(), eq.getFirstMultiplier(), eq.getSecondValue());
            if (convertedEquation != null)
                System.out.println(converter.convert(
                    eq.getFirstValue(), eq.getFirstMultiplier(), eq.getSecondValue()).toString());
        });
    }

    public static void readAndParseEquationsFromConsole(
            List<ValueEquation> equations, List<ValueEquation> equationsToBeFound) {
        Parser equationParser = new Parser();
        try (var reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String equationString = reader.readLine();
                if (equationString == null || equationString.isBlank()) break;
                ValueEquation equation = equationParser.parseLineToEquation(equationString);
                if (equation == null) continue;
                if (equation.getSecondMultiplier() == null)
                    equationsToBeFound.add(equation);
                else {
                    Optional<ValueEquation> eqOption = equations.stream().filter(curEquation -> (
                            curEquation.getFirstValue().equals(equation.getFirstValue())
                            && curEquation.getSecondValue().equals(equation.getSecondValue()))
                            || (curEquation.getFirstValue().equals(equation.getSecondValue())
                            && curEquation.getSecondValue().equals(equation.getFirstValue()))).findAny();
                    if (eqOption.isPresent()) {
                        continue;
                    }
                    equations.add(equation);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error while reading line from console...");
        } catch (IncorrectInputStringException ex) {
            throw new RuntimeException("Incorrect input...");
        }
    }

}
