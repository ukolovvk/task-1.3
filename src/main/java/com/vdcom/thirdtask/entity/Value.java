package com.vdcom.thirdtask.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Value {
    private String name;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj.getClass() != Value.class)
            return false;

        Value otherValue = (Value) obj;
        return this.name.equals(otherValue.getName());
    }

    @Override
    public int hashCode() {
        return 31 * name.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }
}
