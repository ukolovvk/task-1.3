package com.vdcom.thirdtask.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ValueEquation {
    private Value firstValue;
    private Value secondValue;
    private Double firstMultiplier;
    private Double secondMultiplier;

    @Override
    public String toString() {
        return firstMultiplier + " " + firstValue.toString() +
                " = " + secondMultiplier + " " + secondValue.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj.getClass() != ValueEquation.class)
            return false;

        ValueEquation otherEquation = (ValueEquation) obj;
        return this.getFirstValue().equals(otherEquation.getFirstValue())
                && this.getSecondValue().equals(otherEquation.getSecondValue())
                && this.getFirstMultiplier().equals(otherEquation.getFirstMultiplier())
                && this.getSecondMultiplier().equals(otherEquation.getSecondMultiplier());
    }

    @Override
    public int hashCode() {
        int result = firstValue.hashCode();
        return 31 * result + secondValue.hashCode();
    }
}
