package com.vdcom.thirdtask;

import com.sun.tools.javac.Main;
import com.vdcom.thirdtask.converter.Converter;
import com.vdcom.thirdtask.converter.ConverterImpl;
import com.vdcom.thirdtask.entity.Value;
import com.vdcom.thirdtask.entity.ValueEquation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ConverterTest {

    private static final String USER_INPUT = """
            1024 byte = 1 kilobyte
            2 bar = 12 ring
            16.8 ring = 2 pyramid
            4 hare = 1 cat
            5 cat = 0.5 giraffe
            1 byte = 8 bit
            15 ring = 2.5 bar
            1 pyramid = ? bar
            1 giraffe = ? hare
            0.5 byte = ? cat
            2 kilobyte = ? bit
            """;

    private InputStream in = System.in;

    @BeforeEach
    public void init() {
        System.setIn(new ByteArrayInputStream(USER_INPUT.getBytes(StandardCharsets.UTF_8)));
    }

    @AfterEach
    public void replaceInStream() {
        System.setIn(in);
    }

    @Test
    public void taskExampleTest() {
        List<ValueEquation> expected = new ArrayList<>();
        expected.add(new ValueEquation(new Value("pyramid"), new Value("bar"), 1.0, 1.4));
        expected.add(new ValueEquation(new Value("giraffe"), new Value("hare"), 1.0, 40.0));
        expected.add(new ValueEquation(new Value("kilobyte"), new Value("bit"), 2.0, 16384.0));
        List<ValueEquation> equations = new ArrayList<>();
        List<ValueEquation> equationsToBeFound = new ArrayList<>();
        MainClass.readAndParseEquationsFromConsole(equations, equationsToBeFound);
        Converter converter = new ConverterImpl(equations);
        List<ValueEquation> actual = new ArrayList<>();
        equationsToBeFound.stream().forEach(eq -> {
            ValueEquation equation = converter.convert(eq.getFirstValue(), eq.getFirstMultiplier(), eq.getSecondValue());
            if (equation != null) {
                System.out.println(equation);
                actual.add(equation);
            }
        });
        Assertions.assertEquals(expected, actual);
    }
}
