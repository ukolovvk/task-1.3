package com.vdcom.thirdtask.converter;

import com.vdcom.thirdtask.entity.Value;
import com.vdcom.thirdtask.entity.ValueEquation;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.jgrapht.graph.DefaultEdge;
import java.util.List;

public class ConverterImpl implements Converter {

    private Graph<Value, DefaultEdge> valueGraph;

    public ConverterImpl(List<ValueEquation> allEquations) {
        valueGraph = new DefaultDirectedWeightedGraph<>(DefaultEdge.class);
        initGraph(allEquations);
    }

    public ValueEquation convert(Value firstValue, double firstValueMultiplier, Value secondValue) {
        DijkstraShortestPath<Value, DefaultEdge> dijkstraAlg
                = new DijkstraShortestPath<>(valueGraph);
        ShortestPathAlgorithm.SingleSourcePaths<Value, DefaultEdge> path
                = dijkstraAlg.getPaths(firstValue);
        GraphPath<Value, DefaultEdge> graphPath = path.getPath(secondValue);
        if (graphPath == null || graphPath.getEdgeList().size() == 0) {
            System.out.println("Conversion not possible.");
            return null;
        }
        double resultMultiplier =  graphPath.getEdgeList().stream().map(edge -> valueGraph.getEdgeWeight(edge))
                .reduce(1.0, (x, y) -> x * y);
        double secondMultiplier = resultMultiplier * firstValueMultiplier;
        return new ValueEquation(firstValue, secondValue, firstValueMultiplier, secondMultiplier);
    }

    private void initGraph(List<ValueEquation> allEquations) {
        allEquations.forEach(equation -> {
            double weight = equation.getSecondMultiplier() / equation.getFirstMultiplier();
            valueGraph.addVertex(equation.getFirstValue());
            valueGraph.addVertex(equation.getSecondValue());
            DefaultEdge firstEdge = valueGraph.addEdge(equation.getFirstValue(), equation.getSecondValue());
            DefaultEdge secondEdge = valueGraph.addEdge(equation.getSecondValue(), equation.getFirstValue());
            valueGraph.setEdgeWeight(firstEdge, weight);
            valueGraph.setEdgeWeight(secondEdge, (1 / weight));
        });
    }

}
