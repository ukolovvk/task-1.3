package com.vdcom.thirdtask.converter;

import com.vdcom.thirdtask.entity.Value;
import com.vdcom.thirdtask.entity.ValueEquation;

public interface Converter {

    ValueEquation convert(Value firstValue, double firstValueMultiplier, Value secondValue);
}
