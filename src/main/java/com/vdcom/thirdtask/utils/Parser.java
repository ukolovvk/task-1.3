package com.vdcom.thirdtask.utils;

import com.vdcom.thirdtask.entity.Value;
import com.vdcom.thirdtask.entity.ValueEquation;
import com.vdcom.thirdtask.exceptions.IncorrectInputStringException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Parser {

    private List<Value> existedValues;

    public Parser() {
        existedValues = new ArrayList<>();
    }

    public ValueEquation parseLineToEquation(String inputString) throws IncorrectInputStringException {
        String[] partsSpaceSplit = inputString.split(" ");
        if (partsSpaceSplit.length != 5)
            throw new IncorrectInputStringException("Incorrect input string");
        Value firstValue = createOrGetCachedValue(partsSpaceSplit[1]);
        Value secondValue = createOrGetCachedValue(partsSpaceSplit[4]);
        Double firstMultiplier, secondMultiplier;
        try {
            firstMultiplier = Double.parseDouble(partsSpaceSplit[0]);
            if (partsSpaceSplit[3].equals("?"))
                secondMultiplier = null;
            else
                secondMultiplier = Double.parseDouble(partsSpaceSplit[3]);
        } catch (NumberFormatException ex) {
            throw new IncorrectInputStringException("Incorrect input string");
        }
        return new ValueEquation(firstValue, secondValue, firstMultiplier, secondMultiplier);
    }

    private Value createOrGetCachedValue(String valueName) {
        Optional<Value> opValue = existedValues.stream().filter(value -> value.getName()
                .equals(valueName)).findAny();
        return opValue.orElseGet(() -> new Value(valueName));
    }
}
